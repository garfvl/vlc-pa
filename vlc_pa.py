import matplotlib
from PyQt5 import QtCore, QtWidgets
from matplotlib.backends.backend_qt5agg import (
    FigureCanvasQTAgg as FigureCanvas,
    NavigationToolbar2QT as NavigationToolbar
)
from matplotlib.figure import Figure
from threading import Thread, Lock
from queue import Queue, Empty
import argparse
import textwrap
from aiohttp import web
import asyncio
import json
import os
import sys
import logging as log
import time
import numpy as np

matplotlib.use('Qt5Agg')


class VLCLogParser:
    def __init__(self, queue):
        self.queue = queue

    def get_json_label(self, j):
        label = ""
        for key in ["type", "stream", "id", "event"]:
            label += "[{}]".format(j[key]) if key in j else ""
        return label

    def parse_json_log_line(self, line):
        # Parse the json line
        try:
            line_data = json.loads(line)
        except Exception as e:
            log.warning("Cannot parse json log line: " + str(e))
            return

        if type(line_data) != dict:
            log.warning("Unexpected json log line format: " + line)
            return

        if "Timestamp" not in line_data or "Body" not in line_data:
            log.warning("Cannot find Timestamp or Body: " + line)
            return

        ts = line_data["Timestamp"]
        body = line_data["Body"]

        label = self.get_json_label(body)
        if len(label) == 0:
            log.warning("Cannot generate a valid label : " + str(body))
            return

        values = {}
        for k, v in body.items():
            if k in ["type", "stream", "id", "event"]:
                continue
            try:
                values[k] = int(v) / 10 ** 6
            except ValueError:
                try:
                    values[k] = float(v)
                except ValueError:
                    pass

        try:
            its = int(ts) / 10 ** 6
            queue.put((label, its, values))
            log.debug("PUSHING" + str((label, its, values)))
        except ValueError:
            log.warning(
                "Error: some timestamp are not convertible to integer: {}"
                .format(line_data)
            )

    def parse_log_line(self, line):
        # Drop line if it's not an avstat log
        if "avstats:" not in line:
            return
        split = line.split("avstats:")[1].split()
        # find a label in line
        label = split[0]
        if "=" in split:
            log.warning("Cannot find a proper label in avstats: " + line)
            return
        log.debug("STATLINE: " + line)
        values = {}
        for chunk in split[1:]:
            if len(chunk.split("=")) == 2:
                k, v = chunk.split("=")
                try:
                    values[k] = int(v) / 10 ** 6
                except ValueError:
                    log.warning(
                        "Error: some timestamp or value are not convertible"
                        " to integer: {}"
                        .format(chunk)
                    )
                    try:
                        values[k] = float(v)
                    except ValueError:
                        log.warning(
                            "Error: some timestamp or value are not"
                            " convertible to float: {}"
                            .format(chunk)
                        )
        # "ts" aka system timestamp is mandatory
        if "ts" not in values.keys():
            log.warning(" - could not find timestamp for log: " + line)
            return
        ts = values.pop("ts")
        self.queue.put((label, ts, values))
        log.debug("PUSHING " + str((label, ts, values)))


class InputReaderThread(Thread):
    def __init__(self, parser, fd):
        super(InputReaderThread, self).__init__()
        self.parser = parser
        self.fd = fd

    def run(self):
        for line in self.fd:
            self.parser.parse_json_log_line(line)
        log.info("PARSING ENDED")


class HTTPServerThread(Thread):
    def __init__(self, parser, port):
        super(HTTPServerThread, self).__init__()
        self.parser = parser
        self.port = port

    def aiohttp_server(self):
        async def handle_vlc_put(request):
            while True:
                body = await request.content.readline()
                if body == b'':
                    log.info(" --- end of logging")
                    break
                self.parser.parse_json_log_line(body.decode("utf-8"))
            return web.Response(text="")
        app = web.Application()
        app.add_routes([web.put('/', handle_vlc_put)])
        runner = web.AppRunner(app)
        return runner

    def run(self):
        """Run aiohttp in a separate thread,
           to let mathplotlib alone in the main one."""
        runner = self.aiohttp_server()
        loop = asyncio.new_event_loop()
        loop.run_until_complete(runner.setup())
        site = web.TCPSite(runner, '0.0.0.0', self.port)
        loop.run_until_complete(site.start())
        loop.run_forever()


class SafeData:
    def __init__(self):
        self.lock = Lock()
        # init values
        self.values = {}
        self.values_state = 0


def name_from(item):
    if isinstance(item, str):
        return item
    return "{} {}".format(item[0], item[1])


# Converts the output data from the Parser queue to data usable
# by a plot (name -> ([x1, x2, ...] [y1, y2, ...])
class QueueConsumerThread(Thread):
    def __init__(self, queue, data):
        super(QueueConsumerThread, self).__init__()
        self.queue = queue
        self.data = data
        self.running = True

    def run(self):
        while self.running:
            try:
                (label, x, values_dict) = self.queue.get_nowait()
                with self.data.lock:
                    if label not in self.data.values:
                        self.data.values[label] = ([], [])
                    # event only data
                    self.data.values[label][0].append(x)
                    self.data.values[label][1].append(label)
                    for var, v in values_dict.items():
                        name = name_from([label, var])
                        if name not in self.data.values:
                            self.data.values[name] = ([], [])
                        self.data.values[name][0].append(x)
                        self.data.values[name][1].append(v)
                        self.data.values_state += 1
                self.queue.task_done()
            except Empty:
                # let other tasks run
                time.sleep(0.000001)


class PlotCanvas(FigureCanvas):

    def __init__(self):
        self.fig = Figure()
        super(PlotCanvas, self).__init__(self.fig)
        # two different plot spaces: one for 2D data, one for 1D data
        self.gs = self.fig.add_gridspec(2, 1, hspace=0, height_ratios=[3, 1])
        # Ax1 == left 2D axis
        self.ax1 = self.fig.add_subplot(self.gs[0, 0], zorder=10)
        self.ax1.grid()
        self.ax1.set_xlabel("ts from system clock (ms)")
        self.ax1.set_ylabel("pkt value (ms)")
        self.ax1.tick_params(axis='y', labelcolor="tab:red")
        self.ax1.xaxis.set_major_formatter(
            matplotlib.ticker.StrMethodFormatter('{x:,.0f}')
        )
        self.ax1.yaxis.set_major_formatter(
            matplotlib.ticker.StrMethodFormatter('{x:,.0f}')
        )
        self.annot1 = self.ax1.annotate(
            "", xy=(0, 0), xycoords='figure pixels', xytext=(20, 20),
            textcoords="offset points", bbox=dict(boxstyle="round", fc="w"),
            arrowprops=dict(arrowstyle="->"), zorder=1000
        )
        self.annot1.set_visible(False)

        # Ax2 == right 2D axis
        self.ax2 = self.ax1.twinx()
        self.ax2.set_zorder(11)
        self.ax2.tick_params(axis='y', labelcolor="tab:blue")
        self.ax2.set_ylabel("ts from system clock (ms)")
        self.ax2.yaxis.set_major_formatter(
            matplotlib.ticker.StrMethodFormatter('{x:,.0f}')
        )
        self.annot2 = self.ax2.annotate(
            "", xy=(0, 0), xycoords='figure pixels', xytext=(20, 20),
            textcoords="offset points", bbox=dict(boxstyle="round", fc="w"),
            arrowprops=dict(arrowstyle="->"), zorder=1000
        )
        self.annot2.set_visible(False)

        self.curves = {}
        self.curve_dispatch = {}
        self.auto_add = True  # add curve automatically
        self.legend1 = None
        self.legend2 = None
        self.reforge_legends()

        # Add 1D curves
        self.axevents = self.fig.add_subplot(
            self.gs[1, 0], sharex=self.ax1, yticks=[], yticklabels=[])
        self.axevents.grid()
        self.annotevent = self.axevents.annotate(
            "", xy=(0, 0), xycoords='figure pixels', xytext=(20, 20),
            textcoords="offset points", bbox=dict(boxstyle="round", fc="w"),
            arrowprops=dict(arrowstyle="->"), zorder=1000
        )
        self.annotevent.set_visible(False)
        self.axevents.set_visible(False)
        self.gs.set_height_ratios([3, 0.1])

        self.mpl_connect("motion_notify_event", self.hover)
        self.fig.tight_layout()

    def config(self, config):
        self.auto_add = False
        for item in config["left"]:
            name = name_from(item)
            self.curve_dispatch[name] = "left"
        for item in config["right"]:
            name = name_from(item)
            self.curve_dispatch[name] = "right"
        for name in config["events"]:
            self.curve_dispatch[name] = "events"

    def add_curve(self, axis, name, x, y):
        xs = np.array(x)
        if axis == "left":
            ys = np.array(y)
            [self.curves[name]] = self.ax1.plot(
                xs, ys, marker='o', label=name, markersize=3, zorder=10
            )
        elif axis == "right":
            ys = np.array(y)
            [self.curves[name]] = self.ax2.plot(
                xs, ys, marker='x', label=name, markersize=3, zorder=11
            )
        elif axis == "events":
            ys = np.array(y, dtype='U')  # force string type
            # make axevents visisble
            self.axevents.set_visible(True)
            self.gs.set_height_ratios([3, 1])
            [self.curves[name]] = self.axevents.plot(
                xs, ys, marker='o', label=name, markersize=3, zorder=10,
                linestyle='None'
            )
            event_labels = self.get_event_labels()
            self.axevents.set_yticks(range(len(event_labels)))
            self.axevents.set_yticklabels(event_labels)
        self.reforge_legends()

    def get_event_labels(self):
        return [name for name in self.curves.keys()
                if name in self.curve_dispatch.keys()
                and self.curve_dispatch[name] == "events"]

    def remove_curve(self, name):
        axis = self.curve_dispatch.pop(name)
        if axis == "events" and len(self.get_event_labels()) == 0:
            self.axevents.set_visible(False)
            self.gs.set_height_ratios([3, 0.1])
        self.reforge_legends()

    def update_plot(self, name, x, y):
        if name not in self.curves:
            if self.auto_add:
                axis = "left"
                if len(y) > 0 and isinstance(y[0], str):
                    # Not a numerical value
                    axis = "events"
                self.curve_dispatch[name] = axis
                self.add_curve(axis, name, x, y)
            elif not self.auto_add and name in self.curve_dispatch:
                self.add_curve(self.curve_dispatch[name], name, x, y)
        else:
            xs = np.array(x)
            ys = np.array(y)
            self.curves[name].set_data(xs, ys)

    def toggle_legend(self):
        self.legend1.set_visible(not self.legend1.get_visible())
        self.legend2.set_visible(not self.legend2.get_visible())
        self.draw_idle()

    def reforge_legends(self):
        vis = self.legend1.get_visible() if self.legend1 else True
        self.legend1 = self.ax1.legend(loc='upper left')
        self.legend1.set_visible(vis)
        self.legend1.set_zorder(100)
        vis = self.legend2.get_visible() if self.legend2 else True
        self.legend2 = self.ax2.legend(loc='upper right')
        self.legend2.set_visible(vis)
        self.legend2.set_zorder(100)

    def update_annot(self, annot, pos, index_map):
        annot.xy = pos
        textlines = []
        for name, indexes in index_map.items():
            for ind in indexes:
                textlines.append("{}: ({}, {})".format(
                    name, self.curves[name].get_xdata()[ind],
                    self.curves[name].get_ydata()[ind])
                )
                break  # only take the first one
        annot.set_text("\n".join(textlines))
        annot.get_bbox_patch().set_facecolor('w')
        # annot.get_bbox_patch().set_alpha()

    def hover(self, event):
        annotMap = {self.ax1: self.annot1,
                    self.ax2: self.annot2,
                    self.axevents: self.annotevent}
        if (event.inaxes is None
                or event.inaxes not in [self.ax1, self.ax2, self.axevents]):
            for an in annotMap.values():
                if an is not None and an.get_visible():
                    an.set_visible(False)
                    self.draw_idle()
            return
        annot = annotMap[event.inaxes]
        others = [
            an for ax, an in annotMap.items()
            if ax != event.inaxes and an is not None
        ]
        index_map = {}
        for name, curve in self.curves.items():
            cont, ind = curve.contains(event)
            if cont and curve.get_visible() and "ind" in ind.keys():
                index_map[name] = ind["ind"]
        if len(index_map) > 0:
            self.update_annot(annot, (event.x, event.y), index_map)
            annot.set_visible(True)
            for an in others:
                an.set_visible(False)
            self.draw_idle()
        else:
            if annot.get_visible():
                annot.set_visible(False)
                for an in others:
                    an.set_visible(False)
                self.draw_idle()

    def rescale(self, redraw=True):
        self.ax1.relim(visible_only=True)
        self.ax2.relim(visible_only=True)
        self.axevents.relim(visible_only=True)
        self.ax1.autoscale_view()
        self.ax2.autoscale_view()
        self.axevents.autoscale_view()
        if redraw:
            self.draw()


def create_hline():
    line = QtWidgets.QFrame()
    line.setFrameShape(QtWidgets.QFrame.HLine)
    return line


class MainWindow(QtWidgets.QMainWindow):

    def __init__(self, *args, **kwargs):
        super(MainWindow, self).__init__(*args, **kwargs)

    def initialize(self, config, queue):
        self.queue = queue
        self.data = SafeData()
        self.last_values_state = -1
        self.consumer_thread = QueueConsumerThread(self.queue, self.data)
        self.consumer_thread.start()
        self.auto_add = True

        # Init plot canvas
        self.canvas = PlotCanvas()

        # Create toolbar, passing canvas as first parameter,
        # parent (self, the MainWindow) as second.
        navtoolbar = NavigationToolbar(self.canvas, self)
        navtoolbar.addAction('Rescale', self.canvas.rescale)
        navtoolbar.addAction('Toggle Legend', self.canvas.toggle_legend)
        navtoolbar.addAction('Quit', self.close)
        self.addToolBar(navtoolbar)

        layout = QtWidgets.QGridLayout()
        layout.addWidget(self.canvas, 0, 0)

        # add plot list dock
        dock = QtWidgets.QDockWidget("Plots")
        self.addDockWidget(QtCore.Qt.LeftDockWidgetArea, dock)
        scroll = QtWidgets.QScrollArea()
        dock.setWidget(scroll)
        content = QtWidgets.QWidget()
        scroll.setWidget(content)
        scroll.setWidgetResizable(True)
        scroll.setMinimumWidth(350)
        vlayout = QtWidgets.QVBoxLayout(content)
        self.boxes = {}
        self.counters = {}
        self.box_widgets = {}

        # Create "All" group
        g = QtWidgets.QGroupBox("All")
        vlayout.addWidget(g)
        boxlist = QtWidgets.QGridLayout(g)
        boxlist.setVerticalSpacing(0)
        box = QtWidgets.QCheckBox('All Curves')
        box.setChecked(True)
        box.stateChanged.connect(self.togglevisibilityall)
        boxlist.addWidget(box, 0, 0)

        for cat in ["left", "right", "events"]:
            g = QtWidgets.QGroupBox(cat)
            vlayout.addWidget(g)
            boxlist = QtWidgets.QGridLayout(g)
            boxlist.setVerticalSpacing(0)
            self.box_widgets[cat] = (g, boxlist)
            g.setVisible(False)
        vlayout.insertStretch(-1, 1)

        if config is not None:
            self.canvas.config(config)
            self.config(config)

        # Create a placeholder widget to hold our toolbar and canvas.
        widget = QtWidgets.QWidget()
        widget.setLayout(layout)
        self.setCentralWidget(widget)

        # We need to store a reference to the plotted line
        # somewhere, so we can apply the new data to it.
        self.update()
        self.show()

        # Setup a timer to trigger the redraw by calling update.
        self.timer = QtCore.QTimer()
        self.timer.setInterval(16)  # no more than 60fps
        self.timer.timeout.connect(self.update)
        self.timer.start()

    def config(self, config):
        self.auto_add = False
        for cat in ["left", "right", "events"]:
            if cat not in config or len(config[cat]) == 0:
                continue
            for plot in config[cat]:
                name = name_from(plot)
                self.add_box(cat, name)

    def add_box(self, cat, name):
        group, boxlist = self.box_widgets[cat]
        self.boxes[name] = QtWidgets.QCheckBox(name)
        self.boxes[name].setChecked(True)
        if name in self.canvas.curves:
            self.boxes[name].setStyleSheet(
                "color: {}".format(self.canvas.curves[name].get_color()))
        self.boxes[name].stateChanged.connect(self.togglevisibility)
        self.boxes[name].setDisabled(True)
        self.boxes[name].listen = True
        i = boxlist.rowCount()
        boxlist.addWidget(self.boxes[name], i, 0)
        self.counters[name] = QtWidgets.QLabel('0')
        boxlist.addWidget(
            self.counters[name], i, 1, alignment=QtCore.Qt.AlignRight)
        if not group.isVisible():
            group.setVisible(True)

    def resizeEvent(self, event):
        QtWidgets.QMainWindow.resizeEvent(self, event)
        self.canvas.fig.tight_layout()

    def closeEvent(self, event):
        self.consumer_thread.running = False
        event.accept()

    def togglevisibilityall(self):
        b = self.sender()
        check = b.isChecked()
        for name in self.canvas.curves:
            self.boxes[name].listen = False
            self.boxes[name].setChecked(check)
            self.canvas.curves[name].set_visible(check)
            if check:
                self.canvas.curves[name].set_label(name)
            else:
                self.canvas.curves[name].set_label('_Hidden')
        for name in self.boxes.keys():
            self.boxes[name].listen = True
        self.canvas.reforge_legends()
        self.canvas.draw()

    def togglevisibility(self):
        b = self.sender()
        if not b.listen:
            return
        name = b.text()
        check = b.isChecked()
        self.canvas.curves[name].set_visible(check)
        if check:
            self.canvas.curves[name].set_label(name)
        else:
            self.canvas.curves[name].set_label('_Hidden')
        self.canvas.reforge_legends()
        self.canvas.draw()

    def update_counter(self, name, x, y):
        if name not in self.counters:
            if not self.auto_add:
                return
            axis = "left"
            if len(y) > 0 and isinstance(y[0], str):
                # Not a numerical value
                axis = "events"
            self.add_box(axis, name)
        if len(x) > 0:
            self.boxes[name].setDisabled(False)
        self.counters[name].setText(str(len(x)))
        if name in self.canvas.curves:
            self.boxes[name].setStyleSheet(
                "color: {}".format(self.canvas.curves[name].get_color())
            )

    def update(self):
        with self.data.lock:
            if self.last_values_state != self.data.values_state:
                for name in self.data.values:
                    [x, y] = self.data.values[name]
                    self.canvas.update_plot(name, x, y)
                    self.update_counter(name, x, y)
                self.canvas.rescale(redraw=True)
                self.last_values_state = self.data.values_state


if __name__ == '__main__':
    # Parse arguments
    args_parser = argparse.ArgumentParser(
        formatter_class=argparse.RawTextHelpFormatter,
        description=textwrap.dedent("""\
            Generate interactive plots based on VLC stat logs.

            Logs will be read from either stdin (default), input file or HTTP stream."""))
    args_parser.add_argument(
        'input', nargs='?', type=argparse.FileType('r'), default=sys.stdin,
        help="VLC log file"
    )
    args_parser.add_argument(
        '--http', action='store_true',
        help="Launch a HTTP server and listen to the incoming logs"
    )
    args_parser.add_argument(
        '--port', type=int, default=8080, help="HTTP server listen port")
    args_parser.add_argument(
        '--log', type=str, default="INFO", help="Log Verbosity")
    args = args_parser.parse_args()
    # Configure logging
    num_log_level = getattr(log, args.log.upper(), None)
    if not isinstance(num_log_level, int):
        raise ValueError('Invalid log level: %s' % num_log_level)
    log.basicConfig(level=num_log_level)
    # load config json file
    config = None
    default_configfile = "config.json"
    if os.path.isfile(default_configfile):
        with open(default_configfile, "r") as f:
            config = json.load(f)
    # Queue will be used as a FIFO between log parsing and Data plot
    queue = Queue()
    parser = VLCLogParser(queue)
    reader_thread = None
    if args.http:
        reader_thread = HTTPServerThread(parser, args.port)
        reader_thread.start()
    else:
        reader_thread = InputReaderThread(parser, args.input)
        reader_thread.start()
    app = QtWidgets.QApplication(sys.argv)
    w = MainWindow()
    w.initialize(config, queue)
    app.exec_()
