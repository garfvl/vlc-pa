
var configURL = '/config.json';

var resources = []
var data = {datasets: []}
var labels = []
var deriv_label = []
var eventLabels = []
var labelNames = []
var myChart = null
var config = null
var newData = false
var derivatives = []
var events = []

var defaultPointRadius = 2.5
var defaultLineWidth = 1.5

var keepOnlyLastValues = false
var lastSeconds = 0.0

// full data store
var dataMap = {}

function initData() {
  for (resource of resources) {
    label = resource[0]
    labels.push(label)
  }
  for (derivative of derivatives) {
    label = derivative[0]
    deriv_label.push(label)
  }
  for (ev of events) {
    label = ev[0]
    eventLabels.push(label)
  }
}

const repTypes = {
  ONE_DIMENSION: '1D',
  TWO_DIMENSIONS_LINE: '2DLine',
}

function addCurve(name, yAxis, type) {
  let randomColor = '#'+Math.floor(Math.random()*16777215).toString(16)
  labelNames.push(name)
  dataMap[name] = []
  
  // Add in the dataset
  data.datasets.push({
    label: name,
    data: dataMap[name],
    backgroundColor: randomColor,
    borderColor: randomColor,
    showLine: (type != repTypes.ONE_DIMENSION),
    lineTension: 0,
    fill: false,
    yAxisID: yAxis,
    pointRadius: defaultPointRadius,
    pointStyle: (type == repTypes.ONE_DIMENSION) ? 'cross' : 'point',
    borderWidth: defaultLineWidth,
    repType: type // custom field, not from chartjs. Used for representation interactions (hide/show points, etc.)
  })

  console.log("Adding plot: " + name)

  // Add in the table stat
  let tbody = document.getElementById("statsBody")
  let row = tbody.insertRow()
  let cell = row.insertCell()
  let btn = document.createElement("BUTTON")
  btn.setAttribute("type", "button")
  btn.setAttribute("class", "btn btn-primary")
  btn.setAttribute("style", "background-color:"+randomColor+";color:"+textContrastColor(randomColor)+";")
  btn.id = name + "Button"
  btn.setAttribute("name", name)
  btn.setAttribute("onclick", "toggleDataset(this)")
  btn.innerHTML = name
  cell.appendChild(btn)
  cell = row.insertCell()
  cell.id = name + "Points"
  cell.innerHTML = "0"
  cell = row.insertCell()

  console.log("Adding stat row: " + name)
}

function textContrastColor(hex) {
  // assume color is #abcdef
  hex = hex.slice(1);
  let r = parseInt(hex.slice(0, 2), 16),
      g = parseInt(hex.slice(2, 4), 16),
      b = parseInt(hex.slice(4, 6), 16);
  // http://stackoverflow.com/a/3943023/112731
  return (r * 0.299 + g * 0.587 + b * 0.114) > 186 ? '#000000' : '#FFFFFF';
}

/* Check if the function exist and add it if not */
function checkFunctionExistence(name, data_set, index, attribute, type) {
  ii = labelNames.indexOf(name)
  if (ii == -1) {
    // Add a new curb
    if (type == repTypes.ONE_DIMENSION) {
      // Add a new Axis, then a new curve
      let axisId = addYAxis(index, name)
      addCurve(name, axisId, type)
    } else if (data_set[index][1].includes(attribute))
      addCurve(name, 'y', type)
    else if (data_set[index][2].includes(attribute))
      addCurve(name, 'y2', type)
    else
      console.log(name)
  }
}

/* Add a new value to the function at the index places in the resources array */
function updateFunction(index, msg) {
  for (v of resources[index][1].concat(resources[index][2])) {
    if (v in msg) {
      var name = msg.label + " " + v

      checkFunctionExistence(name, resources, index, v, repTypes.TWO_DIMENSIONS_LINE)

      //myChart.data.datasets[ii].data.push({ x: msg.timestamp, y: msg[v]})
      if (keepOnlyLastValues) {
        cutDataFrom(name, msg.timestamp - (lastSeconds * 1000))
      }

       /* Values are sent from the vlc parser under a string type to
         keep int64 precision. */
      var value = parseInt(msg[v])
      var ts = Number(BigInt(msg.timestamp) / BigInt('1000000'))

      dataMap[name].push({ x: ts, y: value })
      updateTableStat(name)
    }
  }
}

/* Calculate a new value to the derivative of the function
 * at the index places in the derivatives array */
function updateDerivative(index, msg) {
  for (v of derivatives[index][1].concat(derivatives[index][2])) {
    if (v in msg) {
      var function_name = msg.label + " " + v
      var deriv_name = msg.label + " derivative " + v

      checkFunctionExistence(deriv_name, derivatives, index, v, repTypes.TWO_DIMENSIONS_LINE)

      //myChart.data.datasets[ii].data.push({ x: msg.timestamp, y: msg[v]})
      if (keepOnlyLastValues) {
        cutDataFrom(name, msg.timestamp - (lastSeconds * 1000))
      }
      var n = dataMap[function_name].length
      if (n > 1) {
        /* Values are sent from the vlc parser under a string type to
         keep int64 precision. */
        var value = parseInt(msg[v])
        var ts = Number(BigInt(msg.timestamp) / BigInt('1000000'))

        var deriv_val = (value - dataMap[function_name][n-2].y) /
                        (ts - dataMap[function_name][n-2].x)
        dataMap[deriv_name].push({ x: ts, y: deriv_val})
        updateTableStat(deriv_name)
      }
    }
  }
}

// Update an Event dataset
function updateEvent(index, msg) {
  var name = msg.label

  checkFunctionExistence(name, null, index, null, repTypes.ONE_DIMENSION)

  if (keepOnlyLastValues) {
    cutDataFrom(name, msg.timestamp - (lastSeconds * 1000))
  }
  dataMap[name].push({ x: msg.timestamp, y: name})
  updateTableStat(name)
}

// Add a stacked Vertical Axis, based on an integer index
function addYAxis(id, label) {
  let realIndex = 3+id // y2 is already taken
  let yAxisID = "y" + String(realIndex)
  config.options.scales[yAxisID] = {
    type: 'category',
    labels: [label],
    position: 'left',
    offset: true,
    stack: 'mainstack',
    stackWeight: 0.1,
    weight: realIndex * -1,
  }

  // add a fake axis to align the right part of the graph
  config.options.scales[yAxisID+"fake"] = {
    type: 'category',
    labels: [label],
    position: 'right',
    offset: true,
    stack: 'mainstack',
    stackWeight: 0.1,
    weight: realIndex, // weight seems inverted for the right part of the graph
  }
  return yAxisID
}

fetch(configURL).then(res => res.json()).then((json) => {
  resources = json.resources
  derivatives = json.derivatives
  events = json.events

  initData()

  // chartjs initial config
  config = {
    type: 'scatter',
    data: data,
    options: {
      animation: false,
      scales: {
        x: {
          type: 'linear',
          position: 'bottom'
        },
        y: {
          type: 'linear',
          position: 'left',
          stack: 'mainstack',
          stackWeight: 1,
          weight: 0,
          ticks: {
            color: '#ff0000'
          }
        },
        y2: {
          type: 'linear',
          position: 'right',
          ticks: {
            color: '#0000ff'
          },
          grid: {
            drawOnChartArea: false
          },
          stack: 'mainstack',
          stackWeight: 1,
          weight: 0,
        }
      },
      plugins: {
        zoom: {
          //pan: {
          //  enabled: true,
          //},
          zoom: {
            wheel: {
              enabled: true,
            },
            drag: {
              enabled: true,
            },
          }
        },
        annotation: {
          annotations: {
          }
        },
        legend: {
          position: "top",
          align: "center",
          labels: {
            filter: function(item) {
              return !item.hidden
            }
          },
          onClick: toggleStats
        },
        tooltip: {
          callbacks: {
            label: function(context) {
                let label = context.dataset.label
                label += ": (" + String(context.parsed.x) + " , " + String(context.parsed.y) + ")"
                return label
            }
          }
        }
      },
      transitions: {
        zoom: {
          animation: {
            duration: 0
          }
        }
      },
    }
  }

  myChart = new Chart(
    document.getElementById('myChart'),
    config
  )

  if ("WebSocket" in window) {
    let l = window.location;
    let wsURL = ((l.protocol === "https:") ? "wss://" : "ws://") + l.hostname + (((l.port != 80) && (l.port != 443)) ? ":" + l.port : "") + "/ws";
    let ws = new WebSocket(wsURL);
    ws.onopen = function() {
      //ws.send("data");
    };
    ws.onmessage = function(evt) {
      var msg = JSON.parse(evt.data)
      var i = labels.indexOf(msg.config_label)
      var i_deriv = deriv_label.indexOf(msg.config_label)
      var iEvent = eventLabels.indexOf(msg.config_label)

      if (iEvent >= 0) {
        updateEvent(iEvent, msg)
        newData = true
        return
      }

      if (i == -1) { return }

      updateFunction(i, msg)
      if (i_deriv >= 0) {
        updateDerivative(i_deriv, msg)
      }

      newData = true

      //myChart.update()
      //ws.send("data");
    };
    ws.onclose = function() {
      console.log("Connection is closed...");
    };
  } else {
    console.log("WebSocket NOT supported by your Browser!");
  }

}).catch(err => { throw err })

// Update chart at max 10fps
var intervalId = setInterval(function() {
  if (newData) {
    newData = false
    myChart.update()
  }
}, 100);

function addVerticalLine(position, label) {
  config.options.plugins.annotation.annotations[label] = {
    type: 'line',
    xMin: position,
    xMax: position,
    borderColor: 'rgb(255, 99, 132)',
    borderWidth: 1,
    borderDash: [10, 10],
    label: {
      enabled: true,
      content: label,
      position: "start",
      rotation: 90
    }
  }
  myChart.update()
}

function clipData(){
  let min = parseInt(document.getElementById("min").value);
  if (isNaN(min)) { min = 0 }
  let max = parseInt(document.getElementById("max").value);
  console.log("Min: " + min)
  console.log("Max: " + max)
  myChart.data.datasets.forEach(function(dataset) {
    let name = dataset.label
    let sliceStart = dataMap[name].findIndex((point) => { return point.x >= min })
    if (sliceStart == -1) { sliceStart = 0 }
    if (isNaN(max)) { max = dataset.data.length }
    let sliceEnd = dataMap[name].findIndex((point) => { return point.x > max })
    if (sliceEnd == -1) { sliceEnd = dataset.data.length }
    console.log("sliceStart: " + sliceStart)
    console.log("sliceEnd: " + sliceEnd)
    dataset.data = dataMap[name].slice(sliceStart, sliceEnd)
  })
  myChart.update()
}

function cutDataFrom(name, startTime) {
  let cutStart = dataMap[name].findIndex((point) => { return point.x >= startTime })
  if (cutStart == -1) { return }
  dataMap[name].splice(0, cutStart)
}

function resetDataClip(){
  myChart.data.datasets.forEach(function(dataset) {
    let name = dataset.label
    dataset.data = dataMap[name]
  })
  myChart.update()
}

function clipDataFromZoom() {
  document.getElementById("min").value = myChart.scales.x.options.min
  document.getElementById("max").value = myChart.scales.x.options.max
  clipData()
}

function toggleAllDatasets(btn) {
  if (btn.classList.contains("btn-primary")) {
    btn.classList.remove("btn-primary")
    btn.classList.add("btn-outline-secondary")
    for (name in dataMap) {
      hideDataset(name)
    }
  } else {
    btn.classList.remove("btn-outline-secondary")
    btn.classList.add("btn-primary")
    for (name in dataMap) {
      showDataset(name)
    }
  }
}

function toggleDataset(btn) {
  if (btn.classList.contains("btn-primary"))
    hideDataset(btn.name)
  else
    showDataset(btn.name)
}

function showDataset(name) {
  let i = labelNames.indexOf(name)
  if (i < 0) return
  myChart.show(i)
  btn = document.getElementById(name + "Button")
  btn.classList.remove("btn-outline-secondary")
  btn.classList.add("btn-primary")
  let color = myChart.data.datasets[i].backgroundColor
  btn.setAttribute("style", "background-color:"+color+";color:"+textContrastColor(color)+";")
}

function hideDataset(name) {
  let i = labelNames.indexOf(name)
  if (i < 0) return
  myChart.hide(i)
  btn = document.getElementById(name + "Button")
  btn.classList.remove("btn-primary")
  btn.classList.add("btn-outline-secondary")
  btn.setAttribute("style", "")
}

function updateTableStat(name){
  let tableCell = document.getElementById(name + "Points")
  tableCell.textContent = String(dataMap[name].length)
}

function togglePoints() {
  myChart.data.datasets.forEach(function(dataset) {
    if (dataset.repType != repTypes.TWO_DIMENSIONS_LINE) return
    if (dataset.pointRadius == 0) {
      dataset.pointRadius = defaultPointRadius
    } else {
      dataset.pointRadius = 0
    }
  })
  myChart.update()
}

function toggleLines() {
  myChart.data.datasets.forEach(function(dataset) {
    if (dataset.repType != repTypes.TWO_DIMENSIONS_LINE) return
    dataset.showLine = !dataset.showLine 
  })
  myChart.update()
}

function toggleKeepOnlyLastValues() {
  lastSeconds = parseInt(document.getElementById("lastSeconds").value)
  keepOnlyLastValues = document.getElementById("checkKeepOnly").checked
}

function toggleSidebar() {
  let side = document.getElementById("sideCol")
  side.hidden = !side.hidden
  resizeMainCol()
}

function toggleStats() {
  let stats = document.getElementById("statsCol")
  stats.hidden = !stats.hidden
  resizeMainCol()
}

function resizeMainCol() {
  let side = document.getElementById("sideCol")
  let stats = document.getElementById("statsCol")
  let main = document.getElementById('mainCol')
  if (side.hidden && stats.hidden) { main.className = "col-11" }
  else if (!side.hidden && !stats.hidden) { main.className = "col-6" }
  else { main.className = "col-9" }
}
